<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exam;

class ExamController extends Controller
{
	public function __construct(){
        $this->middleware('auth');
    }	
        public function index(){
        $exams = Exam::paginate(10);
        return view('exam.index', ['exams' => $exams]);
    }
}
