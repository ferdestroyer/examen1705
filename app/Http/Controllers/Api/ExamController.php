<?php

namespace App\Http\Controllers\API;

use App\Exam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExamController extends Controller
{


    public function show($id)
    {
        $exams = Exam::find($id)->with('modules');
        if ($exams) {
            return $exams;
        }else{
            return response()->json(['message' => 'Record not found'], 404);
        }
    }

    public function destroy($id)
    {
        $exams = Exam::find($id);
        if ($exams) {
            $exams->delete();
            return ['delete' => $id];
        }else{
            return response()->json(['message' => 'Record not found'], 404);
        }
    }
    


}
