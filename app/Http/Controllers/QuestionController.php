<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Module;

class QuestionController extends Controller
{
    public function index(){
        $questions = Question::all();
        return view('question.index', ['questions' => $questions]);
    }

        public function create()
    {   
        $modules = Module::all();
        return view('question.create', [
            'modules' => $modules
        ]);
    }
}
