<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\User;
use App\Module;
class Exam extends Authenticatable
{
	use Notifiable;
    protected $fillable = ['id', 'title', 'date', 'user_id', 'module_id'];
    public function questions(){

        return $this->belongsToMany('\App\Question','exam_question')
            ->withPivot('question_id')->withTimestamps(); 

    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function module()
    {
        return $this->belongsTo('App\Module', 'module_id');
    }

}
