<?php

namespace App;
use App\Exam;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
     protected $fillable = ['id', 'code', 'name'];

}

