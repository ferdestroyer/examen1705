@extends('layouts.app')
@section('content')
<h1>Lista de preguntas</h1>
<table class="table table-bordered">
    <tr>
        <th>pregunta</th>
        <th>A</th>
        <th>B</th>
        <th>C</th>
        <th>D</th>
        <th>Respuesta</th>
        <th>Modulo</th>
    </tr>

    @foreach ($questions as $question)
    <tr>
        <td>{{ $question->text }}</td>
        <td>{{ $question->a }}</td>
        <td>{{ $question->b }}</td>
        <td>{{ $question->c }}</td>
        <td>{{ $question->d }}</td>
        <td>{{ $question->answer }}</td>
        <td>{{ $question->module_id }}</td>
        
    </tr>
    @endforeach
</table>

<input class="btn btn-primary" type="button" value="Nuevo">

@endsection