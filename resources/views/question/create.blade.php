@extends('layouts.app')
@section('content')
<h1>Alta de preguntas</h1>

<form method="post" action="/questions">
    {{ csrf_field() }}

    <div class="form-group">
    <label>Pregunta</label>
    <input type="text" name="title" class="form-control">
    </div>

    <div class="form-group">
    <label>A</label>
    <input type="text" name="a" class="form-control">
    </div>
    <label>B</label>
    <input type="text" name="b" class="form-control">
    </div>
    <label>C</label>
    <input type="text" name="c" class="form-control">
    </div>
    <label>D</label>
    <input type="text" name="d" class="form-control">
    </div>
    <label>Respuesta</label>
    <input type="text" name="answer" class="form-control">
    </div>
    <div class="form-group">
        <label>Modulo</label>
        <br>
        <select name="module_id">
            @foreach ($modules as $module)
            <option value="{{$module->id}}">{{$module->name}}</option>
            @endforeach
        </select>
    </div>


    <div class="form-group">
        <label></label>
        <input class="form-control"  type="submit" name="" value="Nuevo">
    </div>
</form>

@endsection