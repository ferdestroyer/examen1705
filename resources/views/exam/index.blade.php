@extends('layouts.app')
@section('content')
<h1>Lista de recursos</h1>
<table class="table table-bordered">
    <tr>
        <th>id</th>
        <th>titulo</th>
        <th>fecha</th>
        <th>usuario</th>
        <th>modulo</th>
        <th>Acciones</th>
    </tr>

    @foreach ($exams as $exam)
    <tr>
        <td>{{ $exam->id }}</td>
        <td>{{ $exam->title }}</td>
        <td>{{ $exam->date }}</td>
        <td>{{ $exam->user->name }}</td>
        <td>{{ $exam->module->name }}</td>
        <td>
            <input class="btn btn-primary" type="button" value="Ver">
            <input class="btn btn-warning" type="button" value="Editar">
            <input class="btn btn-danger" type="button" value="Eliminar">
        </td>
    </tr>
    @endforeach
</table>

<input class="btn btn-primary" type="button" value="Nuevo">


{{ $exams->links() }}
@endsection